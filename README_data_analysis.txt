Because my version of Excel on my own computer is old and crashes
frequently, and because I wanted to leave the lab computers for others
who were collecting data for Lab 1, I did my data analysis on google
spreadsheets.  I exported the spreadsheet as a .xlsx file, which is in
this Lab 1 repository.  This file included everything except for the
correct data plot formatting.  To see my original file including how I
made the plots included in my lab report, please see my google
spreadsheet at the following link:

https://docs.google.com/spreadsheets/d/1W4B3k-KVKYkwNFv1UI8tTbfDKwigP6lEbW3e_q-NSYM/edit?usp=sharing
